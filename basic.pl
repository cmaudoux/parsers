#!/usr/bin/perl

##############################################################
############ Christophe MAUDOUX - 03/20/2021 #################
### Perl Parser to convert CTU13 scenarios into ARFF files ###
##############################################################

# Copyright 2021 @ Christophe Maudoux.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

use strict;
use Cwd qw(cwd);

# Dur Proto SrcAddr Sport DstAddr Dport TotPkts TotBytes SrcBytes Bot
#  0    1      2      3      4      5      6       7        8      9

my @line;
my $string;
my $first = 1;
my $dir = ( split '/', uc cwd )[-1];
my $proto = qr/^(?:tc|ud)p$/o;
my $addr  = qr/^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/o;

while (<>) {
    chomp;
    @line = map {
        $string =
          $_ =~ $addr
          ? sprintf( "%03i%03i%03i%03i", $1, $2, $3, $4 )
          : $_;
    } grep /\w/, split ',', $_;
    shift @line;
    next
      unless $#line eq 12
      && $line[1] =~ $proto
      && "$line[2]$line[4]" =~ /^\d{24}$/;
    $line[12] = $line[12] =~ /botnet/i ? 'true' : 'false';
    splice @line, 6, 3;
    print "\@relation CTU13-$dir-basic-parsing\n" if $first;
    print <DATA>;
    print "'", join( "','", @line ), "'\n";
    undef $first;
}

__DATA__
@attribute Dur numeric
@attribute Proto {tcp, udp}
@attribute SrcAddr numeric
@attribute Sport numeric
@attribute DstAddr numeric
@attribute Dport numeric
@attribute TotPkts numeric
@attribute TotBytes numeric
@attribute SrcBytes numeric
@attribute Bot {true, false}
@data
