# Combined-Forest: a New Supervised Approach for a Machine-Learning-based Botnets Detection
[C. Maudoux, S. Boumerdassi, A. Barcello & E. Renault](https://hal.archives-ouvertes.fr/hal-03502868v1)

## Perl scripts for CTU13 dataset convertion

These scripts aim at converting or aggregating CTU13 scenarios into
[Weka ARFF files](https://waikato.github.io/weka-wiki/formats_and_processing/arff_stable/).

## Download CTU13 dataset scenarios

All scenarios can be found here:
[Stratosphere CTU13](https://www.stratosphereips.org/datasets-ctu13).

Download the *captureXXXXXXXX.pcap.netflow.labeled* file relative to
each scenario in a **specific directory** named 'SXY' where 'XY'
corresponds to the scenario number (e.g: 42 to 54).

## Launch convertion or aggregation scripts
### Basic convertion: *basic.pl*

This script can be used for converting CTU13 Argus captures into
an ARFF file that can be imported into Weka for processing.

```
basic.pl S42/captureXXXXXXXX.pcap.netflow.labeled > S42/basic.arff
```

### Advanced aggregation: *advanced.pl*

This script can be used for aggregating CTU13 Argus IP-Flows captures
into a Traffic-Flows ARFF file that can be imported into Weka to be processed.

```
advanced.pl S42/captureXXXXXXXX.pcap.netflow.labeled > S42/advanced.arff
```

IP-Flows are aggregated into Traffic-Flows by using a 4-tuples primary key
based on source/destination IPs, destination port and transport protocol (TCP/UDP)
to highlight botnet Command & Control Server.

## Dependencies

No extra Perl module is required.

## The CTU-13 Dataset. A Labeled Dataset with Botnet, Normal and Background traffic.

"An empirical comparison of botnet detection methods" Sebastian Garcia, Martin Grill,
Jan Stiborek and Alejandro Zunino. Computers and Security Journal, Elsevier.
2014. Vol 45, pp 100-123. [ScienceDirect](http://dx.doi.org/10.1016/j.cose.2014.05.011).

