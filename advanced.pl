#!/usr/bin/perl

############################################################################
################# Christophe MAUDOUX - 03/20/2021 ##########################
### Perl Parser to aggregate CTU13 scenarios IP-Flows into Traffic-Flows ###
############################################################################

# Copyright 2021 @ Christophe Maudoux.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

use strict;
use Cwd qw(cwd);

# Dur Proto SrcAddr Sport DstAddr Dport TotPkts TotBytes SrcBytes Bot
#  0    1      2      3      4      5      6       7        8      9

my @line;
my $dir   = ( split '/', uc cwd )[-1];
my $proto = qr/^(?:tc|ud)p$/o;
my $addr  = qr/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/o;
my ( $mvs, $hash ) = ( "','", {} );

while (<>) {
    chomp;
    @line = grep /\w/, split ',', $_;
    shift @line;
    next
      unless $#line eq 12
      && $line[1] =~ $proto
      && $line[2] =~ $addr
      && $line[4] =~ $addr;
    $line[12] = $line[12] =~ /botnet/i ? '1' : '0';
    splice @line, 7, 2;

    my $key = "$line[2]-$line[4]:$line[5]_$line[1]";
    $hash->{$key}->{TotReq}++;
    $hash->{$key}->{TotDur}      += $line[0];
    $hash->{$key}->{TotPkts}     += $line[7];
    $hash->{$key}->{TotBytes}    += $line[8];
    $hash->{$key}->{TotSrcBytes} += $line[9];
    $hash->{$key}->{TotBots}     += $line[10];
    $hash->{$key}->{Dport} = $line[5];
    $hash->{$key}->{Proto} = $line[1];
    $hash->{$key}->{TotAPRSF}->{$_}++
      foreach grep /[APRSF]/, split //, ( split '_', $line[6] )[0];
}

print "\@relation CTU13-$dir\n";
print <DATA>;
print map {
    ;
    "'", $hash->{$_}->{Proto},
      $mvs, $hash->{$_}->{TotAPRSF}->{P} || '0',
      $mvs, $hash->{$_}->{TotAPRSF}->{A} || '0',
      $mvs, $hash->{$_}->{TotAPRSF}->{R} || '0',
      $mvs, $hash->{$_}->{TotAPRSF}->{S} || '0',
      $mvs, $hash->{$_}->{TotAPRSF}->{F} || '0',
      $mvs, $hash->{$_}->{Dport},
      $mvs, $hash->{$_}->{TotReq},
      $mvs, $hash->{$_}->{TotDur},
      $mvs, $hash->{$_}->{TotPkts},
      $mvs, $hash->{$_}->{TotBytes},
      $mvs, $hash->{$_}->{TotSrcBytes},
      $mvs, $hash->{$_}->{TotBots}
      ? "true'\n"
      : "false'\n"
} keys %$hash;

__DATA__
@attribute Proto {tcp, udp}
@attribute PSH numeric
@attribute ACK numeric
@attribute RST numeric
@attribute SYN numeric
@attribute FIN numeric
@attribute Dport numeric
@attribute TotReq numeric
@attribute TotDur numeric
@attribute TotPkts numeric
@attribute TotBytes numeric
@attribute TotSrcBytes numeric
@attribute Bot {true, false}
@data
